//cpp Reference Project

#include <iostream>     // std::cout, std::endl
#include <iomanip>      // std::setw
#include <type_traits>
#include <typeinfo>

template<typename T>
void innerFunction(T &input) {
	std::cout << input << ", LValue \n";
}

template<typename T>
void innerFunction(T &&input) {
	std::cout << input << ", RValue \n";
}

// Below function's parameter is a universal reference.
// T && can be deduced as eighter "T &&" or "T &" (reference collapsing)
template<typename T>
void function(T &&input) {
	innerFunction(std::forward<T>(input));
}

int main(int argc, char **argv) {

	int textColumnWidth { 20 };
	int int1 { 1 };
	float float1 { 1.1 };
	float float2 { 2.2 };

	std::cout << "int1";
	std::cout << std::setw(textColumnWidth + 2) << ": ";
	function(int1);
	std::cout << "int2";
	std::cout << std::setw(textColumnWidth + 2) << ": ";
	function(2);

	std::cout << "float1";
	std::cout << std::setw(textColumnWidth) << ": ";
	function(float1);
	std::cout << "float2";
	std::cout << std::setw(textColumnWidth) << ": ";
	function(float2);
	std::cout << "float1 + float2";
	std::cout << std::setw(textColumnWidth - 9) << ": ";
	function(float1 + float2);

	//string literals are LValues
	std::cout << "Sample String";
	std::cout << std::setw(textColumnWidth - 7) << ": ";
	function("Sample String");

	std::cout << "Moved Sample String";
	std::cout << std::setw(textColumnWidth - 13) << ": ";
	function(std::move("Moved Sample String"));

	std::cout << "\nMAIN END" << std::endl;
	return 0;
}

